//package com.university.ravi.sprintBootUniversityPractice.configuration;
//
//import com.university.ravi.sprintBootUniversityPractice.entity.Student;
//import com.university.ravi.sprintBootUniversityPractice.repository.StudentRepository;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.time.LocalDate;
//import java.time.Month;
//
//@Configuration
//public class StudentConfiguraton {
//
//    @Bean
//    CommandLineRunner commandLineRunner(StudentRepository studentRepository) {
//        return args -> {
//            Student shubhu = new Student(
//                    LocalDate.of(1998, Month.JANUARY,28),
//                    "Shubhashini Bharti",
//                    "Singh",
//                    false
//            );
//            studentRepository.save(shubhu);
//        };
//    }
//}
