package com.university.ravi.sprintBootUniversityPractice.repository;

import com.university.ravi.sprintBootUniversityPractice.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
}
