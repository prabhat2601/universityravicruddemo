package com.university.ravi.sprintBootUniversityPractice.repository;

import com.university.ravi.sprintBootUniversityPractice.entity.Mark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarkRepository extends JpaRepository<Mark, Long> {
}
