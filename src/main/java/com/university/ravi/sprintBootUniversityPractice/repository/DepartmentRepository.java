package com.university.ravi.sprintBootUniversityPractice.repository;

import com.university.ravi.sprintBootUniversityPractice.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {

    @Query("select d from Department d where d.departmentName like concat('%', ?1, '%')")
    List<Department> findByDepartmentNameContaining(String departmentName);
}
