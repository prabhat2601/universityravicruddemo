package com.university.ravi.sprintBootUniversityPractice.repository;

import com.university.ravi.sprintBootUniversityPractice.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long> {

}
