package com.university.ravi.sprintBootUniversityPractice.repository;

import com.university.ravi.sprintBootUniversityPractice.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
