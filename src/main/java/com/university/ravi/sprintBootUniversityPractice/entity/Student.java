package com.university.ravi.sprintBootUniversityPractice.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;

@Entity
@Table(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "student_id_generator")
    @SequenceGenerator(name = "student_id_generator", sequenceName = "student_id_generator", allocationSize = 1)
    @Column(name = "student_id", nullable = false)
    private long id;

    @Column(name = "student_dob", nullable = false)
    private LocalDate dateOfBirth;

    @Column(name = "student_name", nullable = false)
    private String name;

    @Column(name = "student_surname", nullable = false)
    private String surname;

    @Column(name = "student_gender", nullable = false)
    private boolean gender;

    @ManyToOne(
            cascade = CascadeType.ALL,
            optional = false
    )
    @JoinColumn(name = "group_id_fkey", referencedColumnName = "group_id")
    private Group group;

    @Transient //this will not create a column in database
    private int age;

    public Student() {
    }

    public Group getGroup() {
        return group;
    }

    public Student(LocalDate dateOfBirth, String name, String surname, boolean gender, Group group) {
        this.dateOfBirth = dateOfBirth;
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.group = group;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public int getAge() {
        return Period.between(this.dateOfBirth, LocalDate.now()).getYears();
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", dateOfBirth=" + dateOfBirth +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", gender=" + gender +
                ", group=" + group +
                ", age=" + age +
                '}';
    }
}
