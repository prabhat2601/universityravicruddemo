package com.university.ravi.sprintBootUniversityPractice.entity;

import javax.persistence.*;

@Entity
@Table(name = "subject")
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subject_id_generator")
    @SequenceGenerator(name = "subject_id_generator", sequenceName = "subject_id_generator", allocationSize = 1)
    @Column(name = "subject_id", nullable = false)
    private long id;

    @Column(name = "subject_title", nullable = false)
    private String subjectTitles;

    public Subject() {
    }

    public Subject(String subjectTitles) {
        this.subjectTitles = subjectTitles;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSubjectTitles() {
        return subjectTitles;
    }

    public void setSubjectTitles(String subjectTitles) {
        this.subjectTitles = subjectTitles;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", subjectTitles='" + subjectTitles + '\'' +
                '}';
    }
}
