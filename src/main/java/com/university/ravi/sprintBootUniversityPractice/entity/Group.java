package com.university.ravi.sprintBootUniversityPractice.entity;

import javax.persistence.*;

@Entity
@Table(name = "university_group")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "group_id_generator")
    @SequenceGenerator(name = "group_id_generator", sequenceName = "group_id_generator", allocationSize = 1)
    @Column(name = "group_id", nullable = false)
    private Long groupId;

    private String groupName;

    @ManyToOne
    @JoinColumn(name = "department_id", referencedColumnName = "department_id", nullable = false)
    private Department department;

    public Group() {
    }

    public Group(String groupName, Department department) {
        this.groupName = groupName;
        this.department = department;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Group{" +
                "groupId=" + groupId +
                ", groupName='" + groupName + '\'' +
                ", department=" + department +
                '}';
    }
}
