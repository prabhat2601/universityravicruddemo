package com.university.ravi.sprintBootUniversityPractice.entity;

import javax.persistence.*;

@Entity
@Table(name = "marks_of_student")
public class Mark {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mark_id_generator")
    @SequenceGenerator(name = "mark_id_generator", sequenceName = "mark_id_generator", allocationSize = 1)
    @Column(name = "mark_id", nullable = false)
    private Long id;

    @Column(name = "mark_score", nullable = false)
    private Double markScore;

    @ManyToOne(
            cascade = CascadeType.ALL,
            optional = false
    )
    @JoinColumn(
            name = "subject_id_fkey",
            referencedColumnName = "subject_id"
    )
    private Subject subject;

    @ManyToOne(
            cascade = CascadeType.ALL,
            optional = false
    )
    @JoinColumn(
            name = "student_id_fkey",
            referencedColumnName = "student_id"
    )
    private Student student;

    public Mark() {
    }

    public Mark(Double markScore, Subject subject, Student student) {
        this.markScore = markScore;
        this.subject = subject;
        this.student = student;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getMarkScore() {
        return markScore;
    }

    public void setMarkScore(Double markScore) {
        this.markScore = markScore;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return "Mark{" +
                "id=" + id +
                ", markScore=" + markScore +
                ", subject=" + subject +
                ", student=" + student +
                '}';
    }
}
