package com.university.ravi.sprintBootUniversityPractice.exception;

public class DataNotFoundException extends Exception {

    public DataNotFoundException() {
        super("Data Not found");
    }
}
