package com.university.ravi.sprintBootUniversityPractice.controller;

import com.university.ravi.sprintBootUniversityPractice.dto.StudentDto;
import com.university.ravi.sprintBootUniversityPractice.entity.Student;
import com.university.ravi.sprintBootUniversityPractice.exception.DataNotFoundException;
import com.university.ravi.sprintBootUniversityPractice.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/university/student")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    //getting all the student
    @GetMapping
    public List<Student> getAllStudent() {
        return studentService.getAllStudent();
    }

    //getting the student by Id
    @GetMapping(path = "{id}")
    public StudentDto getStudentById(@PathVariable Long id) throws DataNotFoundException {
        return studentService.getStudentById(id);
    }

    //adding student to database
    @PostMapping
    public void addStudentToDatabase(@RequestBody Student student) {
        studentService.addStudentToDatabase(student);
    }

    //updating student in database
    @PutMapping(path = "{idToBeUpdate}")
    public void updateStudentToDatabaseById(@PathVariable Long idToBeUpdate, @RequestBody Student newStudent) {
        studentService.updateStudentToDatabaseById(idToBeUpdate, newStudent);
    }

    //deleting student
    @DeleteMapping(path = "{idToBeDeleted}")
    public void deleteStudentFromDatabaseById(@PathVariable Long idToBeDeleted) {
        studentService.deleteStudentFromDatabaseById(idToBeDeleted);
    }
}
