package com.university.ravi.sprintBootUniversityPractice.controller;

import com.university.ravi.sprintBootUniversityPractice.entity.Department;
import com.university.ravi.sprintBootUniversityPractice.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/university/department")
public class DepartmentController {

    private final DepartmentService departmentService;

    @Autowired
    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @PostMapping
    public void addDepartment(@RequestBody Department department) {
        departmentService.addDepartment(department);
    }

    @GetMapping
    public List<Department> getAllDepartment() {
        return departmentService.getAllDepartment();
    }

    @PutMapping(path = "{departmentIdToBeEdited}")
    public void updateDepartment(@RequestBody Department department, @PathVariable Long departmentIdToBeEdited) {
        departmentService.updateDepartment(departmentIdToBeEdited, department);
    }

    @DeleteMapping(path = "idToBeDeleted")
    public void deleteDepartment(@PathVariable Long idToBeDeleted) {
        departmentService.deleteDepartment(idToBeDeleted);
    }
}
