package com.university.ravi.sprintBootUniversityPractice.service;

import com.university.ravi.sprintBootUniversityPractice.entity.Department;

import java.util.List;

public interface DepartmentService {

    void addDepartment(Department department);

    List<Department> getAllDepartment();

    void updateDepartment(Long departmentIdToBeEdited, Department department);

    void deleteDepartment(Long idToBeDeleted);
}
