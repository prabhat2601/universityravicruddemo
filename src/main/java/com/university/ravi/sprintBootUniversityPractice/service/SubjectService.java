package com.university.ravi.sprintBootUniversityPractice.service;

import com.university.ravi.sprintBootUniversityPractice.entity.Subject;

import java.util.List;

public interface SubjectService {

    List<Subject> getAllSubjects();

}
