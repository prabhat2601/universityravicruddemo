package com.university.ravi.sprintBootUniversityPractice.service.serviceImpl;

import com.university.ravi.sprintBootUniversityPractice.dto.StudentDto;
import com.university.ravi.sprintBootUniversityPractice.entity.Student;
import com.university.ravi.sprintBootUniversityPractice.exception.DataNotFoundException;
import com.university.ravi.sprintBootUniversityPractice.mapper.StudentMapper;
import com.university.ravi.sprintBootUniversityPractice.repository.StudentRepository;
import com.university.ravi.sprintBootUniversityPractice.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ConfigDataNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;

    @Autowired
    public StudentServiceImpl(StudentRepository studentRepository, StudentMapper studentMapper) {
        this.studentRepository = studentRepository;
        this.studentMapper = studentMapper;
    }

    @Override
    public List<Student> getAllStudent() {
        return studentRepository.findAll();
    }

    @Override
    public void addStudentToDatabase(Student student) {
        studentRepository.save(student);
    }

    @Override
    public StudentDto getStudentById(Long id) throws DataNotFoundException {
//        if (studentRepository.findById(id).isPresent()) {
//            Student student = studentRepository.getById(id);
//            return studentMapper.map(student);
//        }

        Optional<Student> student = studentRepository.findById(id);
        return student.map(s -> studentMapper.map(s))
                .orElseThrow(DataNotFoundException::new);
    }

    @Override
    public void updateStudentToDatabaseById(Long idToBeUpdate, Student newStudent) {
        if (studentRepository.findById(idToBeUpdate).isPresent()) {
            studentRepository.save(newStudent);
        } else {
            throw new IllegalArgumentException("Student Does not Exist");
        }
    }

    @Override
    public void deleteStudentFromDatabaseById(Long idToBeDeleted) {
        if (studentRepository.findById(idToBeDeleted).isPresent()) {
            studentRepository.deleteById(idToBeDeleted);
        } else {
            throw new IllegalArgumentException("Student Never Existed");
        }
    }
}
