package com.university.ravi.sprintBootUniversityPractice.service.serviceImpl;

import com.university.ravi.sprintBootUniversityPractice.entity.Department;
import com.university.ravi.sprintBootUniversityPractice.repository.DepartmentRepository;
import com.university.ravi.sprintBootUniversityPractice.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository departmentRepository;

    @Autowired()
    public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Override
    public void addDepartment(Department department) {
        Department departmentToBeSaved = new Department(department.getDepartmentName().toLowerCase());
        List<Department> alreadyPresent = departmentRepository
                .findByDepartmentNameContaining(department.getDepartmentName()
                        .toLowerCase(Locale.ROOT));
        long count = alreadyPresent.size();
        if (count == 0) {
            departmentRepository.save(departmentToBeSaved);
        }
    }

    @Override
    public List<Department> getAllDepartment() {
        return departmentRepository.findAll();
    }

    @Override
    public void updateDepartment(Long departmentIdToBeEdited, Department department) {
        Department departmentToBeSaved = new Department(departmentIdToBeEdited, department.getDepartmentName().toLowerCase());
        if (departmentRepository.findById(departmentIdToBeEdited).isPresent()) {
            departmentRepository.save(departmentToBeSaved);
        } else {
            throw new IllegalArgumentException("No department Present with Id : " + departmentIdToBeEdited);
        }
    }

    @Override
    public void deleteDepartment(Long idToBeDeleted) {
        if (departmentRepository.findById(idToBeDeleted).isPresent()) {
            departmentRepository.deleteById(idToBeDeleted);
        } else {
            throw new IllegalArgumentException("No department Present with Id : " + idToBeDeleted);
        }
    }
}
