package com.university.ravi.sprintBootUniversityPractice.service;

import com.university.ravi.sprintBootUniversityPractice.dto.StudentDto;
import com.university.ravi.sprintBootUniversityPractice.entity.Student;
import com.university.ravi.sprintBootUniversityPractice.exception.DataNotFoundException;

import java.util.List;


public interface StudentService {

    List<Student> getAllStudent();

    void addStudentToDatabase(Student student);

    StudentDto getStudentById(Long id) throws DataNotFoundException;

    void updateStudentToDatabaseById(Long idToBeUpdate, Student newStudent);

    void deleteStudentFromDatabaseById(Long idToBeDeleted);
}
