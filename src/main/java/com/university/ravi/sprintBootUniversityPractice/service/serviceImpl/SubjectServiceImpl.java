package com.university.ravi.sprintBootUniversityPractice.service.serviceImpl;

import com.university.ravi.sprintBootUniversityPractice.entity.Subject;
import com.university.ravi.sprintBootUniversityPractice.repository.SubjectRepository;
import com.university.ravi.sprintBootUniversityPractice.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class SubjectServiceImpl implements SubjectService {

    private final SubjectRepository subjectRepository;

    @Autowired
    public SubjectServiceImpl(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    @Override
    public List<Subject> getAllSubjects() {
        return subjectRepository.findAll();
    }
}
