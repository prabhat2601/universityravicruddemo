package com.university.ravi.sprintBootUniversityPractice.mapper;

import com.university.ravi.sprintBootUniversityPractice.dto.StudentDto;
import com.university.ravi.sprintBootUniversityPractice.entity.Student;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface StudentMapper {

    @Mapping(
            target = "firstName",
            source = "name"
    )
    @Mapping(
            target = "surName",
            source = "surname"
    )
    StudentDto map(Student s);

}