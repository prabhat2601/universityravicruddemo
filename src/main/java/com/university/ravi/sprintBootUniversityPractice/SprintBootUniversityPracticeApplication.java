package com.university.ravi.sprintBootUniversityPractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprintBootUniversityPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SprintBootUniversityPracticeApplication.class, args);
	}

}
